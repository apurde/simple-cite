// "class" to handle all Simple Cite functions
function SimpleCite() {
	
	// render a DOI cite
	this.renderDOI = function(holdingElement) {
		document.getElementById("cite-processor").contentWindow.processBibTeXOrDOI(AJS.$(holdingElement).data("renderTask").doi, AJS.$(holdingElement).data("renderTask").format, function(citeData) {
			afterConversion(holdingElement, citeData);
		}, function(err) {
			finalizeRender(holdingElement);
			insertCite(holdingElement, "Error processing cite: " + err);
		});
	};
	
	
	// render a BibTeX cite
	this.renderBibTeX = function(holdingElement) {
		document.getElementById("cite-processor").contentWindow.processBibTeXOrDOI(AJS.$(holdingElement).data("renderTask").bibTeXEntry, AJS.$(holdingElement).data("renderTask").format, function(citeData) {
			afterConversion(holdingElement, citeData);
		}, function(err) {
			finalizeRender(holdingElement);
			insertCite(holdingElement, "Error processing cite: " + err);
		});
	};
	
	
	// everything we need to do once we got the citation back
	var afterConversion = function(holdingElement, result) {
		finalizeRender(holdingElement);
		insertCite(holdingElement, result.cite);
		overrideReference(holdingElement, result);
		showTooltip(holdingElement);
		cacheResult(result, AJS.$(holdingElement).data("renderTask").cacheKey);
	};

	
	// override citation and show tooltip (if defined)
	this.handleTootipAndOverride = function(holdingElement) {
		showTooltip(holdingElement);
		overrideReference(holdingElement, null);
	};
	
		
	// insert the cite
	var insertCite = function(holdingElement, cite) {
		var insertIntoElement = AJS.$(holdingElement).find(".simple-cite-cite-link").length > 0 ? AJS.$(holdingElement).find(".simple-cite-cite-link:first") : holdingElement; 
		if(AJS.$(insertIntoElement).text().indexOf("$BibTeX") > -1) {
			AJS.$(insertIntoElement).html(AJS.$(insertIntoElement).html().replace("$BibTeX", escapeHtml(cite)));
		}
		else {
			AJS.$(insertIntoElement).text(cite);
		}
	};
	
	
	// cache the result
	var cacheResult = function(result, cacheKey) {
		var citeData = {
			cite : result.cite,
			reference : result.reference
		};
		AJS.$.ajax({
			url: AJS.contextPath() + "/plugins/servlet/simple-cite-cache?cacheKey=" + encodeURI(cacheKey),
			type: "POST",
			contentType: "text/html",
			data : JSON.stringify(citeData)
		});
	};
	
	
	// finalize render 
	var finalizeRender = function(holdingElement) {
		AJS.$(holdingElement).find(".simple-cite-spinner").remove();
	};
	
	
	// override the reference
	var overrideReference = function(holdingElement, result) {
		if(AJS.$(holdingElement).data("citationOverride")) {
			var reference = result ? result.reference : AJS.$(holdingElement).data("reference");
			if(reference.length > 4) {
				AJS.$(".simple-cite-cite[data-cite-id='" + AJS.$(holdingElement).data("citeId") + "']").each(function() {
					AJS.$(this).find("a").text(reference);
				});
			}
		}
	};
	
	
	// show cite as tooltip
	var showTooltip = function(holdingElement) {
		if(AJS.$(holdingElement).data("tooltip")) {
			AJS.$(".simple-cite-cite[data-cite-id='" + AJS.$(holdingElement).data("citeId") + "']").each(function() {
				AJS.$(this).attr("title", AJS.$(holdingElement).html());
				AJS.$(this).tooltip({html:true,className:"simple-cite-tooltip"});
			});
		}
	};
	
	
	// escape HTML (we need this for the cites which contain XSS)
	var escapeHtml = function(html){
		var text = document.createTextNode(html);
		var p = document.createElement('p');
		p.appendChild(text);
		return p.innerHTML;
	};
}


var simpleCite = new SimpleCite();

AJS.toInit(function() {
	
	AJS.$(".simple-cite-render").each(function() {
		if(!AJS.$(this).data("citeRendered")) {
			AJS.$(this).find(".simple-cite-spinner").show();
		}
	});
	
	if(AJS.$("#cite-processor").length === 0) {
		var iframe = document.createElement('iframe');
		iframe.style.display = "none";
		iframe.id="cite-processor";
		iframe.src = AJS.contextPath() + "/download/resources/de.edrup.confluence.plugins.simple-cite/templates/citation-processor.html";
		iframe.onload = function() {
			console.log("Citation processor iframe loaded");
			AJS.$(".simple-cite-render").each(function() {
				if(!AJS.$(this).data("citeRendered")) {
					if(AJS.$(this).data("renderTask").type == "bibTeX") {
						simpleCite.renderBibTeX(this);
					}
					if(AJS.$(this).data("renderTask").type == "doi") {
						simpleCite.renderDOI(this);
					}
				}
				else {
					AJS.$(this).find(".simple-cite-spinner").remove();
					simpleCite.handleTootipAndOverride(this);
				}
			});		
		};
		document.body.appendChild(iframe);
		console.log("Citation processor iframe added");
	}
});