function SimpleCiteBodyHint() {
	
	// init variables
	var observingEditorForCite = false;
	
	
	this.init = function() {
		// check whether we already have the editor
		checkForEditor();
		
		// observe the page once the editPageLink button has been pressed
		AJS.$('#editPageLink').click(function () {
			simpleCiteEditorObserver.observe(document.body, simpleCiteEditorObserverConfig);
		});
	};


	// this functions checks whether the editor is available (we are in edit mode)
	var checkForEditor = function() {
		if(AJS.Rte) {
			if(AJS.Rte.getEditor()) {
				if(AJS.Rte.getEditor().dom) {
					if(AJS.Rte.getEditor().dom.getRoot()) {
						observingEditorForCite = true;
						console.log('simpleCiteBodyOberserver added!');
						simpleCiteBodyObserver.observe(AJS.Rte.getEditor().dom.getRoot(), simpleCiteBodyObserverConfig);
					}
				}
			}
		}
	};
	
	
	// we check for the editor whenever the DOM changes
	var simpleCiteEditorObserver = new MutationObserver(function() {
		if(observingEditorForCite == false) {
			checkForEditor();
		}
	});					
			
	
	// check all changes when editing
	var simpleCiteBodyObserver = new MutationObserver(function(mutations) {
		
		// loop through all mutations reported
		mutations.forEach(function(mutation) {
		
			// loop through all added nodes
  			for(var n = 0; n < mutation.addedNodes.length; n++) {
  			
  				// check whether it is a svg-out macro
  				if(AJS.$(mutation.addedNodes[n]).attr('data-macro-name') == 'single-cite') {
  				
  					// check whether it has an empty body
  					var body = AJS.$(mutation.addedNodes[n]).find('.wysiwyg-macro-body:first');
  					if(AJS.$(body).html() == "<p><br></p>") {
       					console.log("New Simple Cite macro with empty body detected!");
       					
       					// fill the empty body with our hint
       					var bodyHint = AJS.I18n.getText('de.edrup.confluence.plugins.simple-cite.bodyHint');
	       				AJS.$(body).html("<p><span class=\"text-placeholder\">" + bodyHint + "</span></p>");
	       			}
       			}
       		}
        });
	});
	
	
	// options for observing the body of the editor
	var simpleCiteBodyObserverConfig = {
        childList: true,
        subtree: true
	};

	
	// options for observing whether the editor is present
	var simpleCiteEditorObserverConfig = {
        childList: true,
        subtree: true
	};	
}


var simpleCiteBodyHint = new SimpleCiteBodyHint();


AJS.toInit(function() {
	simpleCiteBodyHint.init();
});