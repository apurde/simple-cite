(function($) {
    AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage("bibtex-cite", [ "bib" ]);
    AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage("bibtex-cite-link", [ "bib" ]);
    AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage("bibtex-listing", [ "bib" ]);
})(AJS.$);


