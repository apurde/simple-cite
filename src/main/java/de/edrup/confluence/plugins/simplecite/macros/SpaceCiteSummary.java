package de.edrup.confluence.plugins.simplecite.macros;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.TextQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.upm.api.license.PluginLicenseManager;

import de.edrup.confluence.plugins.simplecite.util.Cite;
import de.edrup.confluence.plugins.simplecite.util.CiteDistinctor;
import de.edrup.confluence.plugins.simplecite.util.CiteManager;

import com.atlassian.confluence.security.Permission;

public class SpaceCiteSummary implements Macro {
	
	private final PageManager pageManager;
	private final PermissionManager permissionManager;
	private final CiteManager citeMan;
	private final PluginLicenseManager pluginLicenseMan;
	private final I18nResolver i18n;
	private final SearchManager searchMan;
	

	@Inject
	public SpaceCiteSummary(@ComponentImport PageManager pageManager, @ComponentImport XhtmlContent xhtmlUtils,
		@ComponentImport SettingsManager settingsManager,
		@ComponentImport PermissionManager permissionManager, CiteManager citeMan,
		@ComponentImport PluginLicenseManager pluginLicenseMan, @ComponentImport I18nResolver i18n,
		@ComponentImport SearchManager searchMan) {
		this.pageManager = pageManager;
		this.permissionManager = permissionManager;
		this.citeMan = citeMan;
		this.pluginLicenseMan = pluginLicenseMan;
		this.i18n = i18n;
		this.searchMan = searchMan;
	}


	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		// check for a valid license
		boolean validLicense = pluginLicenseMan.getLicense().isDefined() ? pluginLicenseMan.getLicense().get().isValid() : false;
		if(!validLicense) {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.license");
		}
		
		try {
			// the current user
			ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
			
			// get parameters
			String space_list = (parameters.containsKey("space_list")) ? parameters.get("space_list") : "";
			String bibTEXFormat = parameters.containsKey("format") ? parameters.get("format") : "ieee";
	
			// split list
			HashSet<String> spacesToCheck = new HashSet<String>();
			if(space_list.length() > 0) {
				spacesToCheck.addAll(Arrays.asList(space_list.split("\\s*,\\s*")));
			}
			
			// in case the list is empty we add the current space
			if(spacesToCheck.size() == 0) {
				spacesToCheck.add(conversionContext.getSpaceKey());
			}
				
			// perform search
			SearchQuery query = BooleanQuery.andQuery(new TextQuery("macroName:single-cite OR macroName:bibtex-cite OR macroName:bibtex-cite-link OR macroName:doi-cite OR macroName:single-cite-import"), new InSpaceQuery(spacesToCheck));
			ContentSearch contentSearch = new ContentSearch(query, null, 0, 1500);;
			SearchResults searchResult = searchMan.search(contentSearch);
			List<SearchResult> searchResultList = searchResult.getAll();
			
			// prepare a list of all cites
			List<Cite> allCites = new ArrayList<Cite>();
			
			// loop through all results
			for(int n = 0; n < searchResultList.size(); n++) {
				
				// determine page
				AbstractPage page = null;
				String pageTitle = "";
				switch(searchResultList.get(n).getType().toLowerCase()) {
					case "blogpost":
						pageTitle = searchResultList.get(n).getDisplayTitle();
						page =  pageTitle.isEmpty() ? null : pageManager.getBlogPost(searchResultList.get(n).getSpaceKey(), pageTitle, date2Calendar(searchResultList.get(n).getCreationDate()));
						break;
					case "page":
						pageTitle = searchResultList.get(n).getDisplayTitle();
						page =  pageTitle.isEmpty() ? null : pageManager.getPage(searchResultList.get(n).getSpaceKey(), pageTitle);
						break;
					default: break;
				}
				
				// is the current user allowed to view the page?
				if((page != null) && permissionManager.hasPermission(confluenceUser, Permission.VIEW, page)) {
					List<Cite> pageCites = citeMan.prepareForSummary(page.getBodyAsString(), bibTEXFormat, conversionContext, page.getEntity(), false);
					allCites.addAll(pageCites);
				}
			}
			
			Collections.sort(allCites);
			
			Map<String, Object> context = MacroUtils.defaultVelocityContext();
			CiteDistinctor cd = new CiteDistinctor();
			context.put("cites", allCites);
			context.put("citeIDs", cd.getDistinctCiteIDs(allCites));
			context.put("cd", cd);
			
			return VelocityUtils.getRenderedTemplate("/templates/space-cite-summary.vm", context);
		}
		catch(Exception e) {
			return e.toString();
		}
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
	
	
	// convert a date to a calender object
	private Calendar date2Calendar(Date date){ 
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}
}
