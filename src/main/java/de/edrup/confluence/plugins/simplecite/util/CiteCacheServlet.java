package de.edrup.confluence.plugins.simplecite.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CiteCacheServlet extends HttpServlet {
	
	private static final long serialVersionUID = 160L;
	
	private final CiteManager citeMan;
	
	@Inject
	public CiteCacheServlet(CiteManager citeMan) {
		this.citeMan = citeMan;
	}
	
	
	// add the entry to the cache
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String cacheKey = URLDecoder.decode(request.getParameter("cacheKey"), "UTF-8");
		String data = convertStreamToString(request.getInputStream());
		if(!data.equals("")) {
			citeMan.insertIntoCache(cacheKey, data);
		}
		
		PrintWriter out = response.getWriter();
		out.print(data);
	}
	
	
	// read the input stream input a string
	private String convertStreamToString(java.io.InputStream is) {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		try {
			while ((length = is.read(buffer)) != -1) {
			    result.write(buffer, 0, length);
			}
			return result.toString("UTF-8");
		}
		catch (Exception e) {
			return "";
		}
	}
}
