package de.edrup.confluence.plugins.simplecite.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.stream.XMLStreamException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.HtmlUtil;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;


@Named
public class CiteManager {
	
	private final XhtmlContent xhtmlUtils;
	private final AttachmentManager attachmentMan;
	private final PermissionManager permissionMan;
	private final PageManager pageMan;
	private final Cache<String, String> cache;
	private final Cache<String, String> staticCache;
	private final I18nResolver i18n;
	private final SettingsManager settingsMan;
	
	private static final Logger log = LoggerFactory.getLogger(CiteManager.class);
	

	@Inject
	public CiteManager(@ComponentImport XhtmlContent xhtmlUtils, @ComponentImport AttachmentManager attachmentMan,
			@ComponentImport PermissionManager permissionMan, @ComponentImport PageManager pageMan,
			@ComponentImport CacheManager cacheMan, @ComponentImport I18nResolver i18n,
			@ComponentImport SettingsManager settingsMan) {
		this.xhtmlUtils = xhtmlUtils;
		this.attachmentMan = attachmentMan;
		this.permissionMan = permissionMan;
		this.pageMan = pageMan;
		this.i18n = i18n;
		this.settingsMan = settingsMan;
		cache = cacheMan.getCache("Simple Cite cache",  null,
			new CacheSettingsBuilder().remote().expireAfterAccess(56, TimeUnit.DAYS).build());
		staticCache = cacheMan.getCache("Simple Cite static cache",  null,
			new CacheSettingsBuilder().remote().expireAfterAccess(56, TimeUnit.DAYS).build());
	}
	
	
	// scan the given content for all single-site macros and build a list of:
	// citeIDs, cites and counts of each cite
	@SuppressWarnings({ "unchecked" })
	public List<Cite> prepareForSummary(String content, String cslFormat, ConversionContext conversionContext, ContentEntityObject ceo, boolean citationOverride) throws MacroExecutionException {

		List<Cite> cites = new ArrayList<Cite>();
		
		// init the counter of cite summaries
		int citeSummaryCount = 0;
		
		// get the last instance number of a cite summary on this page and add one to it => this is our cite summary
		Integer mySummary = conversionContext.hasProperty("de.edrup.confluence.plugins.simple-cite.lastSummary") ?
			(Integer) conversionContext.getProperty("de.edrup.confluence.plugins.simple-cite.lastSummary") + 1 : 1;
		conversionContext.setProperty("de.edrup.confluence.plugins.simple-cite.lastSummary", mySummary);
		
		ArrayList <String> temp_citeIds = new ArrayList<String>();
		ArrayList <Integer> temp_citeCounts = new ArrayList<Integer>();
		
		// get the list of cites and recurrences from the conversion context 
		if(conversionContext.hasProperty("de.edrup.confluence.plugins.simple-cite.citeIDs")) {
			temp_citeIds.addAll((ArrayList <String>) conversionContext.getProperty("de.edrup.confluence.plugins.simple-cite.citeIDs"));
		}
		if(conversionContext.hasProperty("de.edrup.confluence.plugins.simple-cite.citeCounts")) {
			temp_citeCounts.addAll((ArrayList <Integer>) conversionContext.getProperty("de.edrup.confluence.plugins.simple-cite.citeCounts"));
		}
		
		// generate a list of MacroDefinitions from the given content
		final List<MacroDefinition> macros = getMacroDefs(content, conversionContext);
		log.debug("Found {} macros on the content entitity object with id {}", macros.size(), ceo.getId());
		log.debug(content);
		
		// loop through all macros we found and check whether they are a simple-cite(-short) macro
		for(MacroDefinition mdf : macros) {
			
			if(mdf.getName().equals("single-cite-short") || mdf.getName().equals("single-cite") ||
				mdf.getName().equals("bibtex-cite") || mdf.getName().equals("bibtex-cite-link") ||
				mdf.getName().equals("single-cite-import") || mdf.getName().equals("doi-cite")) {
					
				String citeID = mdf.getParameter("citeID");
				String defineOnly = (mdf.getParameters().containsKey("defineOnly")) ?  mdf.getParameter("defineOnly") : "false";
				
				// store citeID and cite only if it is not yet in the list
				if(!cites.contains(new Cite(citeID))) {
					
					Cite cite = new Cite(citeID);
					
					// in case we are handling a single-cite or a bibtex-cite-link
					if(mdf.getName().equals("single-cite") || mdf.getName().equals("bibtex-cite-link")) {
						
						String body = getRenderedBody(mdf, conversionContext);
						
						// just add the body in case of a single cite
						if(mdf.getName().equals("single-cite")) {
							cite.setCite(body);
						}
						
						// add the BibTeX cite inserted into the body in case of bibtext-cite-link
						if(mdf.getName().equals("bibtex-cite-link")) {
							
							// replace the defined string by the BibTeX cite
							if(body.contains("$BibTeX")) {
								handleBibTeXCite(cite, mdf.getParameters(), cslFormat, ceo);
								if(cite.getRendered()) {
									cite.setCite(body.replace("$BibTeX", cite.getCite()));
								}
								else {
									cite.setCite(body);
								}
							}
							else {
								cite.setCite(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.keyword.missing"));
							}
						}
					}
					
					// in case we are handling a short-cite we are facing a use error
					if(mdf.getName().equals("single-cite-short")) {
						cite.setCite(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.short.cite.too.early"));
					}
					
					// in case we are handling BibTeX cite add the cite defined in the BibTeX file
					if(mdf.getName().equals("bibtex-cite")) {
						handleBibTeXCite(cite, mdf.getParameters(), cslFormat, ceo);
					}
					
					// in case we have to handle an imported cite
					if(mdf.getName().equals("single-cite-import")) {
						cite.setCite(importCite(mdf.getParameters(), ceo));
					}
					
					// in case we have to handle a DOI cite
					if(mdf.getName().equals("doi-cite")) {
						handleDOICite(cite, mdf.getParameters(), cslFormat);
					}
					
					cite.setPageTitleLink(String.format("<a href=\"%s\">%s</a>", settingsMan.getGlobalSettings().getBaseUrl() + ceo.getUrlPath(), HtmlUtil.htmlEncode(ceo.getTitle())));
					
					cites.add(cite);
					
					// hold reference in the cache used for static output
					if(!cite.getReference().isEmpty() && citationOverride) {
						staticCache.put(calculateStaticCacheKey(cite.getCiteID(), ceo), cite.getReference());
					}
				}
				
				if(defineOnly.equals("false")) {
					// append "1" in case the cite is in the scope of the local cite summary (defined prior to the current cite summary after the last one)
					// or "0" otherwise
					Boolean inScope = citeSummaryCount == (mySummary - 1);
					Cite citeForScope = cites.get(cites.indexOf(new Cite(citeID)));
					if(citeForScope != null) {
						citeForScope.addToScope(inScope);
						cites.set(cites.indexOf(new Cite(citeID)), citeForScope);
					}
				}
			}	
			
			// in case we reach a cite summary increment the count
			if(mdf.getName().equals("cite-summary")) {
				citeSummaryCount += 1;
			}
		}
		
		return cites;
	}
	
	
	// register a cite and return the number of the cite (starting from 0)
	@SuppressWarnings("unchecked")
	public int registerCiteID(ConversionContext cc, String citeID, Boolean hidePlaceholder) {
		ArrayList <String> temp_citeIDs = new ArrayList<String>();
		ArrayList <Integer> temp_citeCounts = new ArrayList<Integer>();
		
		// get the list of cites and recurrences from the conversion context 
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeIDs")) {
			temp_citeIDs.addAll((ArrayList <String>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeIDs"));
		}
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeCounts")) {
			temp_citeCounts.addAll((ArrayList <Integer>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeCounts"));
		}
		
		// in case the cite is not yet in our list
		if(!temp_citeIDs.contains(citeID)) {
			// add the citeID to the list
			temp_citeIDs.add(citeID);
			// the initial counter would be one
			if(!hidePlaceholder)
				temp_citeCounts.add(1);
			else 
				temp_citeCounts.add(0);
		}
		
		// in case the cite is already in our list
		else {
			// we just increase the recurrences
			temp_citeCounts.set(temp_citeIDs.indexOf(citeID), temp_citeCounts.get(temp_citeIDs.indexOf(citeID)) + 1);
		}
		
		// store the values back in the conversion context for the next macro
		cc.setProperty("de.edrup.confluence.plugins.simple-cite.citeIDs", temp_citeIDs);
		cc.setProperty("de.edrup.confluence.plugins.simple-cite.citeCounts", temp_citeCounts);
		
		// return the number of the cite (starting from 0)
		return temp_citeIDs.indexOf(citeID);
	}
	
	
	// get the current recurrence count of the given citeID
	@SuppressWarnings("unchecked")
	public int getCiteIDCount(ConversionContext cc, String citeID) {
		ArrayList <String> temp_citeIDs = new ArrayList<String>();
		ArrayList <Integer> temp_citeCounts = new ArrayList<Integer>();
		
		// get the list of cites and recurrences from the conversion context 
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeIDs")) {
			temp_citeIDs.addAll((ArrayList <String>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeIDs"));
		}
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeCounts")) {
			temp_citeCounts.addAll((ArrayList <Integer>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeCounts"));
		}
		
		// return the count stored at the position of the given citeID
		return temp_citeCounts.get(temp_citeIDs.indexOf(citeID));
	}
	

	// insert an entry in the cite cache
	public void insertIntoCache(String cacheKey, String data) {
		cache.put(cacheKey, data);
	}
	

	// get all MacroDefinitions of the provided content
	private List<MacroDefinition> getMacroDefs(String content, ConversionContext conversionContext) throws MacroExecutionException {
		// generate a list of MacroDefinitions from the given content
		final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
		try
		{
			xhtmlUtils.handleMacroDefinitions(content, conversionContext, new MacroDefinitionHandler()
			{
				@Override
				public void handle(MacroDefinition macroDefinition) {
					macros.add(macroDefinition);
				}
			});
		}
        catch (XhtmlException e) {
        	log.error(e.toString());
        	throw new MacroExecutionException(e);
        }
		
		return macros;
	}
	
	
	// handle a BibTeX cite
	private void handleBibTeXCite(Cite cite, Map<String, String> bibTeXParams, String cslFormat, ContentEntityObject pageCEO) {

		Attachment bibTeXFile = null;
			
		// get the right content entity object
		ContentEntityObject ceo = findAttachmentsPageCEO(bibTeXParams, pageCEO);
				
		if(ceo != null) {
		
			// check for access rights
			ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
			if(permissionMan.hasPermission(confluenceUser, Permission.VIEW, ceo)) {
				// get the attachment and the format
				if(bibTeXParams.containsKey("name")) {
					bibTeXFile = attachmentMan.getAttachment(ceo, bibTeXParams.get("name"));
				}
			}
			else {
				cite.setCite(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.improper.access.rights"));
			}
		}
		else {
			cite.setCite(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.page.not.found"));
		}
		
		// do we have a proper BibTeX file?
		if(bibTeXFile != null) {
			
			boolean includeLink = (bibTeXParams.containsKey("includeLink")) ?  bibTeXParams.get("includeLink").equals("true") : false;
			
			// generate the cache string (we are looking for that string in the cache)
			// we are using the cache to increase performance
			String cacheKey = bibTeXFile.getIdAsString() + ":" + Integer.toString(bibTeXFile.getVersion()) + ":" + 
				bibTeXParams.get("citeID") + ":" + cslFormat + ":1100";
			
			String bibTeXFileContent = "";
			if(!cache.containsKey(cacheKey) || includeLink) {
				bibTeXFileContent = inputStream2String(attachmentMan.getAttachmentData(bibTeXFile));
			}
			
			// in case we have no hit in cache
			if(cache.get(cacheKey) == null) {
				// BibTeX cite as a "task" for our JS
				String bibTeXEntry = extractBibTeXEntry(bibTeXFileContent, bibTeXParams.get("citeID"));
				if(!bibTeXEntry.isEmpty()) {
					JSONObject renderData = new JSONObject();
					renderData.put("type", "bibTeX");
					renderData.put("format", cslFormat);
					renderData.put("bibTeXEntry", bibTeXEntry);
					renderData.put("cacheKey", cacheKey);
					cite.setTask(renderData.toString());
				}
				else {
					cite.setCite(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.bibtex.notfound"));
				}
			}
			// in case we have a hit in the cache
			else {
				JSONObject dataFromCache = new JSONObject(cache.get(cacheKey));
				cite.setCite(HtmlUtil.htmlEncode(dataFromCache.getString("cite")));
				cite.setReference(dataFromCache.getString("reference"));
			}
			
			// include a link of needed
			if(includeLink) {
				cite.setURL(getBibTeXURL(extractBibTeXEntry(bibTeXFileContent, bibTeXParams.get("citeID"))));
			}
		}
		else {
			cite.setCite(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.access.problem"));
		}
	}
	
	
	// return all the cites in a BibTeX file as a table
	public CiteListAndResult getAllBibTeXCites(Map<String, String> bibTeXParams, ContentEntityObject pageCEO) {
		
		Attachment bibTeXFile = null;
		CiteListAndResult result = new CiteListAndResult();
		
		// get the right content entity object
		ContentEntityObject ceo = findAttachmentsPageCEO(bibTeXParams, pageCEO);
				
		if(ceo != null) {
		
			// check for access rights
			ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
			if(permissionMan.hasPermission(confluenceUser, Permission.VIEW, ceo)) {
				// get the attachment and the format
				if(bibTeXParams.containsKey("name")) {
					bibTeXFile = attachmentMan.getAttachment(ceo, bibTeXParams.get("name"));
				}
			}
			else {
				result.setErrorMessage(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.improper.access.rights"));
				return result;
			}
		}
		else {
			result.setErrorMessage(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.page.not.found"));
			return result;
		}
		
		// do we have a proper BibTeX file?
		if(bibTeXFile != null) {
			try {
				
				// determine the relevant parameters
				String cslFormat = bibTeXParams.containsKey("format") ? bibTeXParams.get("format") : "ieee";
				boolean includeLink = (bibTeXParams.containsKey("includeLink")) ?  bibTeXParams.get("includeLink").equals("true") : false;
				
				// read bib-file content as string
				String bibTeXFileContent = inputStream2String(attachmentMan.getAttachmentData(bibTeXFile));
				
				// get all BibTeX-IDs from the file
				ArrayList<String> bibTeXIDs = new ArrayList<String>();
				bibTeXIDs.addAll(getAllBibTeXIDs(bibTeXFileContent));
				
				// loop through all IDs
				for(int n = 0; n < bibTeXIDs.size(); n++) {
					
					String bibTeXID = bibTeXIDs.get(n);
					String cacheKey = bibTeXFile.getIdAsString() + ":" + Integer.toString(bibTeXFile.getVersion()) + ":" + 
						bibTeXID + ":" + cslFormat + ":1100";
					
					Cite cite = new Cite(bibTeXID);
					
					// in case we have no hit in cache
					if(cache.get(cacheKey) == null) {
					
						// include the BibTeX cite as a "task" for our JS
						JSONObject bibTeXData = new JSONObject();
						bibTeXData.put("type", "bibTeX");
						bibTeXData.put("format", cslFormat);
						bibTeXData.put("bibTeXEntry", extractBibTeXEntry(bibTeXFileContent, bibTeXID));
						bibTeXData.put("cacheKey", cacheKey);
						cite.setTask(bibTeXData.toString());
					}
					
					// in case we have a cache hit
					else {
						JSONObject dataFromCache = new JSONObject(cache.get(cacheKey));
						cite.setCite(dataFromCache.getString("cite"));
						cite.setReference(dataFromCache.getString("reference"));
					}
					
					// include a link of needed
					if(includeLink) {
						cite.setURL(getBibTeXURL(extractBibTeXEntry(bibTeXFileContent, bibTeXID)));
					}
								
					result.getCites().add(cite);
				}
				
				return result;
			}
			
			// in case of an exception
			catch(Exception e) {
				result.setErrorMessage(e.getMessage());
				return result;
			}
			
		}
		else {
			result.setErrorMessage(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.access.problem"));
			return result;
		}
	}
	
	
	// find the right ContentEntityObject
	private ContentEntityObject findAttachmentsPageCEO(Map<String, String> parameters, ContentEntityObject pageCEO) {
		
		// do some init stuff
		ContentEntityObject ceo = null;
		Page page = null;
		
		// find the right page
		// in case the parameter page is not given we take the current entity 
		if(!parameters.containsKey("page")) {
			ceo = pageCEO;
		}
		// in case the parameter page is given
		else {
			// get the parameter page
			String pageParam = parameters.get("page");
			String spaceParam = "";
			
			// is the "unofficial" parameter "space" given?
			// remark: the js provided by Confluence only sets it in case the space is different from the current space
			if(parameters.containsKey("space")) {
				spaceParam = parameters.get("space");
			}
			// in case "space" is not in the list of parameters
			else {
				// we will find the page/draft in our space
				if(pageCEO instanceof Page) {
					spaceParam = ((Page) pageCEO).getSpaceKey();
				}
				if(pageCEO instanceof Draft) {
					spaceParam = ((Draft) pageCEO).getDraftSpaceKey();
				}
			}
			
			// get the page
			page = pageMan.getPage(spaceParam, pageParam);
		
			// in case we found a page get the entity of it
			if(page != null) {
				ceo = page.getEntity();
			}
		}
		
		// return the content entity object we found
		return ceo;
	}
	
	
	// get the cite for the given citeID from another page
	private String importCite(Map<String, String> parameters, ContentEntityObject pageCEO) {
		
		// determine the page we want to import from
		Page p = null;
		if(parameters.get("page").contains(":")) {
			String[] spaceAndPage = parameters.get("page").split(":");
			p = pageMan.getPage(spaceAndPage[0], spaceAndPage[1]);
		}
		else {
			p = pageMan.getPage(((Page) pageCEO).getSpaceKey(), parameters.get("page"));
		}
		
		if(p == null) {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.import.page.notfound");
		}
		
		// try to find the corresponding macro
		try {
			final List<MacroDefinition> macros = getMacroDefs(p.getBodyAsString(), new DefaultConversionContext(p.toPageContext()));
			
			// loop through all macros we found and whether they match our citeID
			for(MacroDefinition mdf : macros) {
				if(mdf.getName().equals("single-cite") && mdf.getParameters().containsKey("citeID") && mdf.getParameter("citeID").equals(parameters.get("citeID"))) {
					return getRenderedBody(mdf, new DefaultConversionContext(p.toPageContext()));
				}
			}
		
			// not found
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.import.cite.notfound");
		}
		catch(Exception e) {
			return e.toString();
		}
	}
	
	
	// create the string which is later picked-up by JS for DOI cites
	private void handleDOICite(Cite cite, Map<String, String> parameters, String cslFormat) {
		
		// cache key
		String cacheKey = parameters.get("citeID") + ":" + cslFormat + ":1100";

		// check if we already have that entry in the cache
		if(cache.get(cacheKey) != null) {
			JSONObject dataFromCache = new JSONObject(cache.get(cacheKey));
			cite.setCite(HtmlUtil.htmlEncode(dataFromCache.getString("cite")));
			cite.setReference(dataFromCache.getString("reference"));
		}
				
		// not in the cache
		else {
			// return the DOI cite as a "task" for our JS
			JSONObject renderData = new JSONObject();
			renderData.put("type", "doi");
			renderData.put("format", cslFormat);
			renderData.put("doi", parameters.get("citeID"));
			renderData.put("cacheKey", cacheKey);
			cite.setTask(renderData.toString());
		}
		
		// add link?
		boolean addLink =  parameters.containsKey("addLink") ? parameters.get("addLink").equals("true") : false;
		if(addLink) {
			cite.setURL("https://dx.doi.org/" +  parameters.get("citeID"));
		}
	}
	
	
	// get the body from the given macro
	private String getRenderedBody(MacroDefinition mdf, ConversionContext context) {
		// get the body
		// this has to be so complex as mdf.getBodyText cuts out macros in the body
		// issue #7
		StringWriter body_sw = new StringWriter();
		try {
			mdf.getBody().getStorageBodyStream().writeTo(body_sw);
		}
		catch(IOException | NoSuchMethodError e) {
			// not all supported versions of Confluence support the above function
			body_sw.write(mdf.getBodyText());
		}
		
		// convert StringWriter to string
		String body = body_sw.toString();
		
		try {
			return xhtmlUtils.convertStorageToView(body, context);
		} catch (XMLStreamException | XhtmlException e) {
			return e.toString();
		}
	}
	
	
	// extract a BibTeX entry from a file provided as String
	private String extractBibTeXEntry(String bibTeXFileContent, String ID) {
		Pattern p = Pattern.compile("@[a-zA-Z ]*\\{" + ID + " *,", Pattern.DOTALL);
		Matcher m = p.matcher(bibTeXFileContent);
		if(m.find()) {
			return bibTeXFileContent.substring(m.start(), findBalancedEnd(bibTeXFileContent, m.start()));
		}
		else {
			return "";
		}
	}
	
	
	// helper function for extracting a BibTeX entry
	private int findBalancedEnd(String str, int startIndex) {
		String searchIn = str.substring(startIndex);
		Pattern p = Pattern.compile("[\\{\\}]", Pattern.DOTALL);
		Matcher m = p.matcher(searchIn);
		int count = 0;
		while(m.find()) {
			count = m.group().equals("{") ? count + 1 : count - 1;
			if(count == 0) {
				return m.end() + startIndex;
			}
		}
		return startIndex;
	}
	
	
	// get all BibTeX IDs from a file provided as String
	private ArrayList<String> getAllBibTeXIDs(String bibTeXFileContent) {
		ArrayList<String> entries = new ArrayList<String>();
		Pattern p = Pattern.compile("@[a-zA-Z ]*\\{.*?,", Pattern.DOTALL);
		Matcher m = p.matcher(bibTeXFileContent);
		while(m.find()) {
			entries.add(m.group().replaceFirst("@.*?\\{", "").replace(",", ""));
		}
		return entries;
	}
	
	
	// extract the URL out of the BibTeX entry
	private String getBibTeXURL(String bibTexEntry) {
		Pattern p = Pattern.compile("url *= *[\\{|\"].*?[\\}|\"]", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(bibTexEntry);
		if(m.find()) {
			return sanitizeUrl(m.group().replaceFirst("(?i)url *= *[\\{|\"]", "").replaceFirst("[\\}|\"]", ""));
		}
		return "";
	}
	
	
	// read the inputstream into a string
	private String inputStream2String(InputStream in) {
		Scanner scanner = new Scanner(in, "UTF-8");
		scanner.useDelimiter("\\A");
		String content = scanner.hasNext() ? scanner.next() : "";
		scanner.close();
		return content;
	}
	
	
	// check if url is valid
	private String sanitizeUrl(String url) {
		return url.replaceAll("(javascript *:)|(data *:)|[\"'<>]", "");
	}
	
	
	// build the key for the static cache
	private String calculateStaticCacheKey(String citeID, ContentEntityObject ceo) {
		return String.format("%s-%d-%d", citeID, ceo.getId(), ceo.getVersion());
	}
	
	
	// get the static override from the cache or an empty string
	public String getStaticOverride(String citeID, ContentEntityObject ceo) {
		String staticCacheEntry = staticCache.get(calculateStaticCacheKey(citeID, ceo));
		return (staticCacheEntry == null) ? "" : staticCacheEntry;
	}
}
