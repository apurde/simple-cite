package de.edrup.confluence.plugins.simplecite.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

import de.edrup.confluence.plugins.simplecite.util.CiteListAndResult;
import de.edrup.confluence.plugins.simplecite.util.CiteManager;

public class BibtexListing implements Macro {
	
	private final CiteManager citeMan;

	
	@Inject
	public BibtexListing(CiteManager citeMan, XhtmlContent xhtmlUtils) {
		this.citeMan = citeMan;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) {
		
		CiteListAndResult result = citeMan.getAllBibTeXCites(parameters, conversionContext.getEntity());
		
		if(result.getSuccess() == false) {
			return result.getErrorMessage();
		}
		else {
			Map<String, Object> context = MacroUtils.defaultVelocityContext();
			context.put("cites", result.getCites());
			return VelocityUtils.getRenderedTemplate("/templates/bibtex-listing.vm", context);
		}
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
