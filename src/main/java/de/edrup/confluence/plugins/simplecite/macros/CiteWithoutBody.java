package de.edrup.confluence.plugins.simplecite.macros;

import javax.inject.Inject;

import de.edrup.confluence.plugins.simplecite.util.CiteManager;

public class CiteWithoutBody extends CiteWithBody {

	@Inject
	public CiteWithoutBody(CiteManager citeMan) {
		super(citeMan);
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}
}
