package de.edrup.confluence.plugins.simplecite.macros;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.FormatConverter;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.upm.api.license.PluginLicenseManager;

import de.edrup.confluence.plugins.simplecite.util.Cite;
import de.edrup.confluence.plugins.simplecite.util.CiteManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CiteSummary implements Macro {

	private final CiteManager citeMan;
	private final FormatConverter formatConverter;
	private final I18nResolver i18n;
	private final PluginLicenseManager pluginLicenseMan;

	private static final Logger log = LoggerFactory.getLogger(CiteSummary.class);

	
	@Inject
	public CiteSummary(@ComponentImport XhtmlContent xhtmlUtils, CiteManager citeMan, @ComponentImport FormatConverter formatConverter,
		@ComponentImport I18nResolver i18n, @ComponentImport PluginLicenseManager pluginLicenseMan) {
		this.citeMan = citeMan;
		this.formatConverter = formatConverter;
		this.i18n = i18n;
		this.pluginLicenseMan = pluginLicenseMan;
	}
	
	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			final ConversionContext conversionContext) throws MacroExecutionException {
		
		// check for a valid license
		boolean validLicense = pluginLicenseMan.getLicense().isDefined() ? pluginLicenseMan.getLicense().get().isValid() : false;
		if(!validLicense) {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.license");
		}
		
		// get the parameters we need
		String bibTEXFormat = parameters.containsKey("format") ? parameters.get("format") : "ieee";
		boolean local = parameters.containsKey("local") ? parameters.get("local").equals("true") : false;
		String showCitesAsTooltip = parameters.containsKey("showCitesAsTooltip") ? parameters.get("showCitesAsTooltip") : "true";
		String citationOverride = parameters.containsKey("citationOverride") ? parameters.get("citationOverride") : "false";
		String enumerationStyle = parameters.containsKey("enumerationStyle") ? parameters.get("enumerationStyle") : "1.";
		boolean showCitationLinks = parameters.containsKey("showCitationLinks") ? parameters.get("showCitationLinks").equals("true") : true;
		
		// prepare the summary
		String body = conversionContext.getEntity().getBodyAsString();
		if(conversionContext.getOutputType().equals("preview") && (!body.contains("cite-summary") || body.contains("data-macro-name"))) {
			try {
				body = formatConverter.convertToStorageFormat(body, conversionContext.getEntity().toPageContext());
			}
			catch(Exception e) {
				log.error(e.toString());
				return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.preview.error");
			}
		}
		List<Cite> cites = citeMan.prepareForSummary(body, bibTEXFormat, conversionContext, conversionContext.getEntity(), "true".equals(citationOverride));

		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("cites", cites);
		context.put("showCitesAsTooltip", showCitesAsTooltip);
		context.put("citationOverride", citationOverride);
		context.put("enumerationStyle", enumerationStyle);
		context.put("showCitationLinks", showCitationLinks);
		context.put("local", local);
		
		return (conversionContext.getOutputType().toLowerCase().equals("word") ||
			"com.k15t.scroll.scroll-office".equals(conversionContext.getProperty("com.k15t.scroll.product.key"))) ?
			VelocityUtils.getRenderedTemplate("/templates/cite-summary-word.vm", context) :
			VelocityUtils.getRenderedTemplate("/templates/cite-summary.vm", context);
	}


	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}	
}
