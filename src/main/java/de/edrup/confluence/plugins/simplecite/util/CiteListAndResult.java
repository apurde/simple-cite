package de.edrup.confluence.plugins.simplecite.util;

import java.util.ArrayList;

public class CiteListAndResult {
	
	private ArrayList<Cite> cites;
	private String errorMessage;
	
	
	public CiteListAndResult() {
		cites = new ArrayList<Cite>();
		errorMessage = "";
	}

	
	public ArrayList<Cite> getCites() {
		return cites;
	}
	
	
	public boolean getSuccess() {
		return errorMessage.isEmpty();
	}
	

	public String getErrorMessage() {
		return errorMessage;
	}
	
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
