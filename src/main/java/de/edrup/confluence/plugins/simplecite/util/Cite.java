package de.edrup.confluence.plugins.simplecite.util;

import java.util.ArrayList;

public class Cite implements Comparable<Cite> {
	
	private String citeID;
	private String cite;
	private String reference;
	private String task;
	private ArrayList<Boolean> scope;
	private String url;
	private Boolean rendered;
	private int count;
	private String pageTitleLink;
	
	
	public Cite() {
		this.citeID = "";
		this.cite = "";
		this.reference = "";
		this.task = "";
		this.scope = new ArrayList<Boolean>();
		this.url = "";
		this.rendered = true;
		this.count = 0;
		this.pageTitleLink = "";
	}
	
	
	public Cite(String citeID) {
		this();
		this.citeID = citeID;
	}
	
	
	@Override
	public boolean equals(Object cite) {
		if(cite instanceof Cite) {
			return ((Cite) cite).citeID.equals(this.citeID);
		}
		else {
			return false;
		}
	}
	
	
	@Override
	public int hashCode() {
		return citeID.hashCode();
	}
	
	
	public String getCiteID() {
		return this.citeID;
	}
	
	
	public String getCite() {
		return cite;
	}
	
	
	public void setCite(String cite) {
		this.cite = cite;
	}

	
	public String getReference() {
		return reference;
	}
	
	
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
	public String getURL() {
		return url;
	}
	
	
	public void setURL(String url) {
		this.url = url;
	}
	
	
	public String getTask() {
		return task;
	}
	
	
	public void setTask(String task) {
		this.task = task;
		setRendered(false);
	}
	
	
	public boolean getRendered() {
		return rendered;
	}
	
	
	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	
	public ArrayList<Boolean> getScope() {
		return scope;
	}
	
	
	public void addToScope(Boolean inScope) {
		scope.add(inScope);
		count++;
	}
	
	
	public int getCount() {
		return count;
	}
	
	
	public String getPageTitleLink() {
		return pageTitleLink;
	}
	
	
	public void setPageTitleLink(String pageTitleLink) {
		this.pageTitleLink = pageTitleLink;
	}


	@Override
	public int compareTo(Cite cCite) {
		int diff = -cCite.getCiteID().compareTo(citeID);
		return (diff != 0) ? diff : -cCite.getCite().compareTo(cite);
	}
	
	
	public boolean deepEquals(Cite cite) {
		return (this.citeID.equals(cite.getCiteID())) &&
			(this.cite.equals(cite.getCite())) &&
			(this.task.equals(cite.getTask())) &&
			(this.reference.equals(this.getReference())) &&
			(this.url.equals(this.getURL()));
	}
}
