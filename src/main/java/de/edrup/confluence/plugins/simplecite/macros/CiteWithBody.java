package de.edrup.confluence.plugins.simplecite.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import de.edrup.confluence.plugins.simplecite.util.CiteManager;


public class CiteWithBody implements Macro {
	
	private final CiteManager citeMan;
	private final String staticOutput = "pdf word";
	
	@Inject
	public CiteWithBody(CiteManager citeMan) {
		this.citeMan = citeMan;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		
		String citeID = parameters.get("citeID");
		boolean superscript = (parameters.containsKey("superscript")) ? parameters.get("superscript").equals("true") : true;
		String citepages = (parameters.containsKey("pages")) ? parameters.get("pages") : "";
		boolean defineOnly = (parameters.containsKey("defineOnly")) ? parameters.get("defineOnly").equals("true") : false;
		
		if(defineOnly) {
			return "";
		}
		
		int citeNo = citeMan.registerCiteID(conversionContext, citeID, defineOnly);
		int citeCount = citeMan.getCiteIDCount(conversionContext, citeID);
		
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("citeID", citeID);
		context.put("citeNo", citeNo + 1);
		context.put("citeCount", citeCount);
		context.put("superscript", superscript);
		context.put("citepages", citepages);
		context.put("staticOverride", staticOutput.contains(conversionContext.getOutputType()) ?
			citeMan.getStaticOverride(citeID, conversionContext.getEntity()) : "");
		
		return VelocityUtils.getRenderedTemplate("/templates/cite.vm", context).trim();
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
