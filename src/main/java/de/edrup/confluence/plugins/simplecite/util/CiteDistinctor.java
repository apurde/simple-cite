package de.edrup.confluence.plugins.simplecite.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CiteDistinctor {
	
	
	public List<String> getDistinctCiteIDs(List<Cite> cites) {
		return cites.stream().map(x -> x.getCiteID()).distinct().collect(Collectors.toList());
	}
	
	
	public List<Cite> getDistinctCitesForCiteID(List<Cite> cites, String citeID) {
		ArrayList<Cite> filteredCites = new ArrayList<Cite>();
		for(Cite cite : cites) {
			if((cite.getCiteID().equals(citeID)) && !deepContains(filteredCites, cite)) {
				filteredCites.add(cite);
			}
		}
		return filteredCites;
	}
	
	
	public List<String> getPageTitleAndLinksForCiteID(List<Cite> cites, String citeID) {
		return cites.stream().filter(x -> x.getCiteID().equals(citeID)).map(x -> x.getPageTitleLink()).collect(Collectors.toList());
	}
	
	
	public int getCiteCountForCiteID(List<Cite> cites, String citeID) {
		return cites.stream().filter(x -> x.getCiteID().equals(citeID)).map(x -> x.getCount()).reduce(0, Integer::sum);
	}
	
	
	private boolean deepContains(List<Cite> cites, Cite compareCite) {
		for(Cite cite : cites) {
			if(cite.deepEquals(compareCite)) {
				return true;
			}
		}
		return false;
	}
}
